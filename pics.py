import tkinter as tk

from PIL import Image, ImageTk

from editor import PycEditor


class PycPics(tk.Frame):
    def __init__(self, *args, **kwargs):
        tk.Frame.__init__(self, *args, **kwargs)
        self.displayed = None

    def __str__(self):
        return 'pycpics'

    def load_initial(self, data):
        if self.displayed:
            self.displayed.forget()

        id, path, timestamp = data
        if str(id) not in self.children:
            self.load(id, path, timestamp)
        image_label = self.children[str(id)]

        image = image_label.resized_image
        if (
                image.width() != self.master.winfo_width() - 2 or
                image.height() != self.master.winfo_height() - 2
        ):
            self.resize(image_label)
        image_label.pack(expand=True, fill=tk.BOTH)
        self.displayed = image_label

    def load(self, id, path, timestamp):
        image_label = tk.Label(self, name=str(id))
        image_label.timestamp = timestamp
        image = Image.open(path)
        image = PycEditor.rotate_pic(image)
        image_label.image = image
        self.resize(image_label)

    def load_prev(self, event):
        data = self._root().db.filter(timestamp__lt=self.displayed.timestamp).last()
        if data:
            self.load_initial(data)

    def load_next(self, event):
        data = self._root().db.filter(timestamp__gt=self.displayed.timestamp).first()
        if data:
            self.load_initial(data)

    def resize(self, image_label):
        image = image_label.image
        width = self.master.winfo_width() - 2
        height = self.master.winfo_height() - 2
        image = PycEditor.resize(image, width, height)
        image_label.resized_image = ImageTk.PhotoImage(image)
        image_label.configure(image=image_label.resized_image)

    def bind_actions(self):
        self.bind('<Configure>', lambda event, image_label=self.displayed: self.resize(image_label))
        self.bind_all('<Up>', self.load_prev)
        self.bind_all('<Down>', self.load_next)
        self.bind_all('<4>', self.load_prev)
        self.bind_all('<5>', self.load_next)
        self.focus_set()

    def unbind_actions(self):
        self.unbind_all('<Configure>')
        self.unbind_all('<Up>')
        self.unbind_all('<Down>')
        self.unbind_all('<4>')
        self.unbind_all('<5>')
