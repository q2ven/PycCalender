import datetime
import os

from PIL import Image

import settings

EXIF_ORIENTATION = 274
EXIF_DATE_TIME_ORIGINAL = 36867
EXIF_DATE_TIME_DIGITIZED = 36868
EXIF_TIMESTAMPS = (EXIF_DATE_TIME_ORIGINAL, EXIF_DATE_TIME_DIGITIZED)
EXIT_TIMESTAMP_FORMAT = '%Y:%m:%d %H:%M:%S'


class PycEditor(object):
    @staticmethod
    def create_thumbnail(image, pixel):
        # keep aspect ratio and shrink
        width, height = image.size
        if width > height:
            width = pixel * width // height
            height = pixel
            offset = (width - pixel) // 2
            box = (offset, 0, width - offset, height)
        else:
            height = pixel * height // width
            width = pixel
            offset = (height - pixel) // 2
            box = (0, offset, width, height - offset)

        image = image.resize((width, height), Image.ANTIALIAS).crop(box)

        # this line may or may not be necessary (crop() is lazy operation)
        image.load()

        return image

    @staticmethod
    def resize(image, width, height):
        # keep aspect ratio and fit to width and height
        i_width, i_height = image.size

        if i_width / width > i_height / height:
            i_height = i_height * width // i_width
            i_width = width
        else:
            i_width = i_width * height // i_height
            i_height = height
        return image.resize((i_width, i_height), Image.ANTIALIAS)

    @staticmethod
    def rotate_pic(image):
        if 'exif' not in image.info:
            return image

        exif = image._getexif()
        if not exif or EXIF_ORIENTATION not in exif:
            return image

        try:
            return {
                1: lambda image: image,
                2: lambda image: image.transpose(Image.FLIP_LEFT_RIGHT),
                3: lambda image: image.transpose(Image.ROTATE_180),
                4: lambda image: image.transpose(Image.FLIP_TOP_BOTTOM),
                5: lambda image: image.transpose(Image.FLIP_TOP_BOTTOM).transpose(Image.ROTATE_90),
                6: lambda image: image.transpose(Image.ROTATE_270),
                7: lambda image: image.transpose(Image.FLIP_TOP_BOTTOM).transpose(Image.ROTATE_270),
                8: lambda image: image.transpose(Image.ROTATE_90),
            }[exif[EXIF_ORIENTATION]](image)
        except:
            return image

    def get_timestamp(self, path):
        try:  # get created time
            timestamp = datetime.datetime.fromtimestamp(
                os.path.getctime(path)
            ).strftime(settings.TIMESTAMP_FORMAT)
        except OSError:
            return  # TODO log unopenable file

        try:
            image = Image.open(path)
        except OSError:
            return timestamp

        if 'exif' not in image.info:
            return timestamp

        exif = image._getexif()
        # EXIF datetime ('%Y:%m:%d %H:%M:%S') needs to be converted for DB
        for key in EXIF_TIMESTAMPS:
            try:
                jpeg_timestamp = datetime.datetime.strptime(
                    exif.get(key, ''), EXIT_TIMESTAMP_FORMAT
                ).strftime(settings.TIMESTAMP_FORMAT)
                return jpeg_timestamp
            except ValueError:
                pass
        return timestamp
