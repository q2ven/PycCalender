import datetime
import time
import tkinter as tk

import settings


class PycDays(tk.Frame):
    def __init__(self, *args, **kwargs):
        tk.Frame.__init__(self, *args, **kwargs)
        self.frames = {}
        self.displayed = []

        # scrollbar must be packed before canvas, or bad look
        self.scrollbar = tk.Scrollbar(self, orient='v')
        self.scrollbar.pack(side=tk.RIGHT, fill=tk.Y)

        self.canvas = tk.Canvas(self)
        self.canvas.pack(expand=True, fill=tk.BOTH)

        self.frame = tk.Frame(self.canvas)
        self.frame.pack(expand=True, fill=tk.BOTH)

        self.scrollbar.config(command=self.canvas.yview)
        self.canvas.configure(yscrollcommand=self.scrollbar.set)
        self.frame_id = self.canvas.create_window((0, 0), window=self.frame, anchor=tk.NW)

    def __str__(self):
        return 'pycdays'

    def load_initial(self, data=None):
        if not data:
            return

        self.unpack_frames()
        self.displayed = []

        self.load(data)
        self.pack_frames()

    def load(self, data):
        id, path, timestamp = data
        timestamp = self.conv_timestamp(timestamp)

        if timestamp not in self.frames:
            pycday = PycDay(self.frame, timestamp=timestamp)
            self.frames[timestamp] = pycday
        else:
            pycday = self.frames[timestamp]
        self.displayed.append(pycday)

    def load_prev(self):
        data = self._root().db.filter(timestamp__lt=self.displayed[0].timestamp).last()
        if data:
            self.unpack_frames()
            self.load(data)
            self.pack_frames()

    def load_next(self):
        data = self._root().db.filter(timestamp__gt=self.displayed[-1].timestamp).first()
        if data:
            self.unpack_frames()
            self.load(data)
            self.pack_frames()

    def pack_frames(self):
        self.displayed.sort(key=lambda frame: frame.timestamp)
        for frame in self.displayed:
            frame.pack(expand=True, fill=tk.BOTH)

    def unpack_frames(self):
        for frame in self.displayed:
            frame.forget()

    def resize(self, event):
        # set the inner frame width
        self.canvas.itemconfig(
            self.frame_id,
            width=self.winfo_width() - self.scrollbar.winfo_width()
        )
        # set the scroll region fitting to the inner frame
        self.canvas.configure(scrollregion=self.canvas.bbox('all'))

    def scroll(self, event):
        # get relative scrollbar position from 0.0 to 1.0
        top, bottom = self.scrollbar.get()

        if event.keysym == 'Up' or event.num == 4:
            if top == 0.0:
                self.load_prev()
                return
            self.canvas.yview('scroll', -1, 'units')
        elif event.keysym == 'Down' or event.num == 5:
            if bottom == 1.0:
                self.load_next()
                return
            self.canvas.yview('scroll', 1, 'units')

    def bind_actions(self):
        self.bind_all('<Configure>', self.resize)
        self.bind_all('<Up>', self.scroll)
        self.bind_all('<Down>', self.scroll)
        self.bind_all('<4>', self.scroll)
        self.bind_all('<5>', self.scroll)
        self.focus_set()

    def unbind_actions(self):
        self.unbind_all('<Configure>')
        self.unbind_all('<Up>')
        self.unbind_all('<Down>')
        self.unbind_all('<4>')
        self.unbind_all('<5>')

    def conv_timestamp(self, timestamp, day=0):
        return datetime.date.fromtimestamp(
            time.mktime(
                (
                    timestamp.year,
                    timestamp.month,
                    timestamp.day + day,
                    0, 0, 0,
                    0, 0, 0,
                )
            )
        )

    def get_pycframe(self, timestamp):
        return self.frames[self.conv_timestamp(timestamp)]


class PycDay(tk.Frame):
    def __init__(self, *args, **kwargs):
        self.timestamp = kwargs.pop('timestamp')
        tk.Frame.__init__(self, *args, **kwargs)

        self.set_daylabel()
        self.set_pic()

    def set_daylabel(self):
        self.day_label = tk.Label(
            self,
            fg='black',
            font=settings.FONT_M,
            text=self.timestamp.strftime('%Y/%m/%d')
        )
        self.grid_rowconfigure(0, minsize=30, weight=0)
        self.grid_columnconfigure(0, weight=settings.PIC_COLUMN)
        self.day_label.grid(
            row=0,
            column=0,
            columnspan=settings.PIC_COLUMN, sticky=tk.N + tk.S + tk.E + tk.W
        )

    def set_pic(self):
        row_offset = 1

        for i, data in enumerate(
                # fetch all at once for local processing
                self._root().db.filter(
                    timestamp__year=self.timestamp.year,
                    timestamp__month=self.timestamp.month,
                    timestamp__day=self.timestamp.day,
                ).all()
        ):
            id = data[0]  # I deliberately write this for future update of DB field.
            image_frame = tk.Frame(self, name=str(id))
            row = row_offset + i // settings.PIC_COLUMN
            column = i % settings.PIC_COLUMN

            # auto resize grid by weight option
            self.grid_rowconfigure(row, minsize=50, weight=0)
            self.grid_columnconfigure(column, minsize=50, weight=1)
            image_frame.grid(row=row, column=column, sticky=tk.N + tk.W, padx=2)

            self._root().io_queue.put(
                (str(PycDays), data)
            )
