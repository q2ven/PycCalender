import multiprocessing
import queue

from PIL import Image

import settings
from days import PycDays
from db import PycDB
from editor import PycEditor
from months import PycMonths


class PycLoader(multiprocessing.Process):
    def __init__(self, cpu_queue, io_queue):
        super().__init__()
        self.cpu_queue = cpu_queue
        self.io_queue = io_queue
        self.db = PycDB(**settings.DB_OPTION)

    def run(self):
        while True:
            try:
                pyctype, data = self.io_queue.get(block=False)
            except queue.Empty:
                continue

            try:
                if pyctype == str(PycDB):
                    self.save_pic(data)  # data: path
                elif pyctype in (str(PycMonths), str(PycDays)):
                    self.open_pic(pyctype, data)  # data: (path, timestamp)
            except Exception as e:  # absorb unexpected errors and avoid stopping
                if settings.DEBUG:
                    print(e)

    def open_pic(self, pyctype, data):
        id, path, timestamp = data
        image = Image.open(path)
        image = PycEditor.rotate_pic(image)
        image = PycEditor.create_thumbnail(image, settings.THUMBNAIL_PIXEL)

        self.cpu_queue.put(
            (pyctype, data, image)
        )

    def save_pic(self, path):
        if len(self.db.filter(path=path)):  # already exist
            return

        timestamp = self.get_timestamp(path)
        if timestamp:
            self.db.save_pic(path, timestamp)
