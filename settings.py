# DB
DB_USER = 'postgres'  # can create db
DB_PASS = 'postgres'
DB_HOST = 'localhost'
DB_NAME = 'testdb'

DB_OPTION = {
    'user': DB_USER,
    'password': DB_PASS,
    'host': DB_HOST,
    'dbname': DB_NAME,
}

TABLE_NAME = 'pycdata'

TIMESTAMP_FORMAT = "%Y-%m-%d %H:%M:%S"

# FILE TYPE
EXTENSIONS = ['.jpg', '.JPG', '.png', '.PNG']
FILETYPES = [
    ('JPG', '.jpg'),
    ('JPG', '.JPG'),
    ('PNG', '.png'),
    ('PNG', '.PNG')
]

# TK
LOAD_INTERVAL = 10
DEBUG = False

# Calendar
FIRST_WEEKDAY = 6  # 0: Monday, 6: Sunday
FONT_M = ('Century Schoolbook L', 15)
THUMBNAIL_PIXEL = 180
PIC_COLUMN = 7
