import glob
import os
import queue
import stat
import tkinter as tk
from tkinter import filedialog

from PIL import ImageTk

import settings
from days import PycDays
from db import PycDB
from months import PycMonths
from pics import PycPics


class PycTk(tk.Tk):
    def __init__(self, cpu_queue, io_queue):
        tk.Tk.__init__(self)

        self.cpu_queue = cpu_queue
        self.io_queue = io_queue
        self.db = PycDB(**settings.DB_OPTION)
        self.frames = {}

        self.title('PycCalendar')
        self.minsize(1030, 640)
        self.set_menubar()
        self.set_baseframes()
        self.after(settings.LOAD_INTERVAL, self.check_queue)

    def run(self):
        self.mainloop()

    def set_menubar(self):
        self.menu = menu = tk.Menu(self)  # create main menu
        self.config(menu=menu)  # bind main menu

        menu.files = tk.Menu(menu, tearoff=False)  # create submenu
        menu.edit = tk.Menu(menu, tearoff=False)

        menu.add_cascade(label='Files', under=0, menu=menu.files)
        menu.add_cascade(label='Edit', under=0, menu=menu.edit)

        menu.files.add_command(
            label='Open a file',
            under=0,
            command=self.askopenfilename
        )
        menu.files.add_command(
            label='Open a directory',
            under=0,
            command=self.askdirectory
        )
        menu.files.add_command(
            label='Quit',
            under=0,
            command=self.destroy
        )

        self.set_fileoption()

    def set_fileoption(self):
        """
        set option for opening file or directory
        """
        # file option
        self.file_opt = options = {}
        options['defaultextension'] = settings.EXTENSIONS[0]
        options['filetypes'] = settings.FILETYPES
        options['initialdir'] = '~/'
        options['parent'] = self
        options['title'] = 'Open a file'

        # directory option
        self.dir_opt = options = {}
        options['initialdir'] = '~/'
        options['mustexist'] = False
        options['parent'] = self
        options['title'] = 'Open a directory'

    def askopenfilename(self):
        """
        get the file name and put it into queue
        """
        filepath = filedialog.askopenfilename(**self.file_opt)

        if filepath:
            # set the dir to be opened next time
            self.file_opt['initialdir'] = os.path.dirname(filepath)
            self.dir_opt['initialdir'] = os.path.dirname(filepath)
            self.io_queue.put(
                ('db', filepath)
            )

    def askdirectory(self):
        """
        get the directory name
        """
        dirpath = filedialog.askdirectory(**self.dir_opt)

        if dirpath:
            # set the dir to be opened next time
            self.file_opt['initialdir'] = dirpath
            self.dir_opt['initialdir'] = dirpath

            # put all path into queue
            self.dirrecursion(dirpath)

    def dirrecursion(self, dirpath):
        """
        put all path in the directory into queue
        """
        try:
            paths = glob.glob(dirpath + '/*')
        except:  # ignoring regex error(sre_constants.error)
            return

        for path in paths:
            status = os.stat(path)
            if stat.S_ISDIR(status.st_mode):
                self.dirrecursion(path)
            else:
                if os.path.splitext(path)[1] in settings.EXTENSIONS:
                    self.io_queue.put(
                        (str(PycDB), path)
                    )

    def set_baseframes(self):
        self.container = tk.Frame(self)
        self.container.pack(expand=True, fill=tk.BOTH)

        for frame in (PycPics, PycDays, PycMonths):
            self.frames[str(frame)] = frame(self.container)

        pycmonths = self.pack_frame(PycMonths)
        pycmonths.load_initial()
        pycmonths.bind_actions()

    def check_queue(self):
        try:
            pyctype, data, image = self.cpu_queue.get(block=False)
        except queue.Empty:
            self.after(settings.LOAD_INTERVAL, self.check_queue)
            return

        id, path, timestamp = data

        if pyctype == str(PycMonths):
            key = str(timestamp.day)
        elif pyctype == str(PycDays):
            key = str(id)

        pycframe = self.frames[pyctype].get_pycframe(timestamp)
        image_frame = pycframe.children[key]

        if hasattr(image_frame, 'image_label'):
            image_label = image_frame.image_label
        else:
            # if PycDays creates image_label in advance,
            # displays pics slowly...
            image_label = tk.Label(image_frame)
            image_label.pack(expand=True, fill=tk.BOTH)

        image = ImageTk.PhotoImage(image)
        image_label.image = image
        image_label.configure(image=image)

        if pyctype == str(PycMonths):
            image_label.bind(
                '<Button-1>',
                lambda event, data=data: self.month_day(data)
            )
        elif pyctype == str(PycDays):
            image_label.bind(
                '<Button-1>',
                lambda event, data=data: self.day_pyc(data)
            )

        self.after(settings.LOAD_INTERVAL, self.check_queue)

    def pack_frame(self, klass, data=None):
        pycframe = self.frames[str(klass)]
        pycframe.pack(expand=True, fill=tk.BOTH)
        if data:
            pycframe.load_initial(data)
        else:
            pycframe.load_initial()
        pycframe.bind_actions()
        return pycframe

    def unpack_frame(self, klass):
        pycframe = self.frames[str(klass)]
        pycframe.forget()
        pycframe.unbind_actions()
        return pycframe

    def month_day(self, data):
        self.unpack_frame(PycMonths)
        pycdays = self.pack_frame(PycDays, data)
        pycdays.bind_all('<Button-3>', self.day_month)

    def day_pyc(self, data):
        self.unpack_frame(PycDays)
        pycpics = self.pack_frame(PycPics, data)
        pycpics.bind_all('<Button-3>', self.pyc_day)

    def pyc_day(self, event):
        self.unpack_frame(PycPics)
        pycdays = self.pack_frame(PycDays)
        pycdays.bind_all('<Button-3>', self.day_month)

    def day_month(self, event):
        self.unpack_frame(PycDays)
        self.pack_frame(PycMonths)
