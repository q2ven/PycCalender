import calendar
import datetime
import time
import tkinter as tk

import settings

WEEKDAY_OPT = {
    0: ('black', 'Mon'),
    1: ('black', 'Tue'),
    2: ('black', 'Wed'),
    3: ('black', 'Thu'),
    4: ('black', 'Fri'),
    5: ('blue', 'Sat'),
    6: ('red', 'Sun'),
}


class PycMonths(tk.Frame):
    def __init__(self, *args, **kwargs):
        tk.Frame.__init__(self, *args, **kwargs)
        self.frames = {}
        self.displayed = None

    def __str__(self):
        return 'pycmonths'

    def load_initial(self):
        if self.displayed:
            return

        data = self._root().db.last()
        if data:
            id, path, timestamp = data
        else:
            timestamp = datetime.date.today()
        timestamp = self.conv_timestamp(timestamp)
        self.load(timestamp)

    def load(self, timestamp):
        if timestamp not in self.frames:
            pycmonth = PycMonth(self, timestamp=timestamp)
            self.frames[timestamp] = pycmonth
        else:
            pycmonth = self.frames[timestamp]
        pycmonth.pack(expand=True, fill=tk.BOTH)
        self.displayed = pycmonth

    def load_prev(self, event):
        timestamp = self.displayed.timestamp
        if not self._root().db.filter(timestamp__lt=timestamp):
            return
        timestamp = self.conv_timestamp(timestamp, month=-1)

        self.displayed.forget()
        self.load(timestamp)

    def load_next(self, event):
        timestamp = self.displayed.timestamp
        timestamp = self.conv_timestamp(timestamp, month=1)
        if not self._root().db.filter(timestamp__gte=timestamp):
            return

        self.displayed.forget()
        self.load(timestamp)

    def bind_actions(self):
        self.bind_all('<Up>', self.load_prev)
        self.bind_all('<Down>', self.load_next)
        self.bind_all('<4>', self.load_prev)
        self.bind_all('<5>', self.load_next)
        self.focus_set()

    def unbind_actions(self):
        self.unbind_all('<Up>')
        self.unbind_all('<Down>')
        self.unbind_all('<4>')
        self.unbind_all('<5>')

    def conv_timestamp(self, timestamp, month=0):
        return datetime.date.fromtimestamp(
            time.mktime(
                (
                    timestamp.year,
                    timestamp.month + month,
                    1,
                    0, 0, 0,
                    0, 0, 0,
                )
            )
        )

    def get_pycframe(self, timestamp):
        return self.frames[self.conv_timestamp(timestamp)]


class PycMonth(tk.Frame):
    def __init__(self, *args, **kwargs):
        self.timestamp = kwargs.pop('timestamp')
        tk.Frame.__init__(self, *args, **kwargs)

        self.set_monthlabel()
        self.set_weekdaylabel()
        self.set_calendar()

    def set_monthlabel(self):  # row: 0
        self.month_label = tk.Label(
            self,
            bd=2,
            relief=tk.RIDGE,
            fg='black',
            font=settings.FONT_M,
            text=self.timestamp.strftime('%Y/%m')
        )
        self.grid_rowconfigure(0, minsize=30, weight=0)
        self.grid_columnconfigure(0, weight=1)
        self.month_label.grid(row=0, column=0, columnspan=7, sticky=tk.N + tk.S + tk.E + tk.W)

    def set_weekdaylabel(self):  # row: 1
        for i in range(7):
            weekday_label = tk.Label(
                self,
                bd=2,
                relief=tk.RIDGE,
                fg=WEEKDAY_OPT[i][0],
                font=settings.FONT_M,
                text=WEEKDAY_OPT[i][1]
            )
            column = (i - settings.FIRST_WEEKDAY) % 7
            self.grid_rowconfigure(1, minsize=50, weight=0)
            self.grid_columnconfigure(column, minsize=50, weight=1)
            weekday_label.grid(row=1, column=column, sticky=tk.N + tk.S + tk.E + tk.W)

    def set_calendar(self):  # row: 2~
        row_offset = 2

        for week_number, dates in enumerate(
            calendar.Calendar(
                settings.FIRST_WEEKDAY
            ).monthdatescalendar(self.timestamp.year, self.timestamp.month)
        ):
            for date in dates:
                if date.month != self.timestamp.month:  # skip date in prev or next month
                    continue

                image_frame = tk.Frame(self, name=str(date.day), bd=2, relief=tk.RIDGE)
                row = row_offset + week_number
                column = (date.weekday() - settings.FIRST_WEEKDAY) % 7

                # auto resize grid by weight option
                self.grid_rowconfigure(row, weight=1)
                self.grid_columnconfigure(column, weight=1)
                image_frame.grid(row=row, column=column, sticky=tk.N + tk.S + tk.E + tk.W)

                image_label = tk.Label(image_frame)
                image_label.pack(expand=True, fill=tk.BOTH)
                image_frame.image_label = image_label

                day_label = tk.Label(
                    image_label,
                    fg=WEEKDAY_OPT[date.weekday()][0],
                    text=str(date.day)
                )
                day_label.pack(anchor=tk.N + tk.W)

                # disable children to make parent expand
                image_frame.pack_propagate(False)

                data = self._root().db.filter(timestamp=date).first()
                if data:
                    self._root().io_queue.put(
                        (str(PycMonths), data)
                    )
