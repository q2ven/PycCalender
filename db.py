import datetime

from psycopg2 import OperationalError, connect, sql
from psycopg2.extensions import (ISOLATION_LEVEL_AUTOCOMMIT,
                                 ISOLATION_LEVEL_DEFAULT)

import settings


class PycDBBase(object):
    """
    Bass class about DB connection
    """

    cursor_name = None

    def __init__(self, user, password, host, dbname):
        self._user = user
        self._password = password
        self._host = host
        self._dbname = dbname

    def _connect(self, option):
        self._con = connect(**option)
        self._cursor = self._con.cursor(name=self.get_cursor_name())

    def connect_db(self):
        self._connect(self.get_option())

    def connect_host(self):
        option = self.get_option()
        option.pop('dbname')
        self._connect(option)

    def get_option(self):
        return {
            'user': self._user,
            'password': self._password,
            'host': self._host,
            'dbname': self._dbname,
        }

    def get_cursor_name(self):
        return self.cursor_name

    def close(self):
        self._cursor.close()
        self._con.close()


class PycDB(PycDBBase):
    """
    DB class with some necessary methods
    """
    def __init__(self, user, password, host, dbname):
        super().__init__(user, password, host, dbname)

        try:
            self.connect_db()
        except OperationalError:
            self.connect_host()
            self.create_db()
            self.close()
            self.connect_db()
            self.create_table()

    def __str__(self):
        return 'pycdb'

    def create_db(self):
        self._con.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
        self._cursor.execute(
            sql.SQL(
                'CREATE DATABASE {};'
            ).format(sql.Identifier(self._dbname))
        )
        self._con.set_isolation_level(ISOLATION_LEVEL_DEFAULT)

    def create_table(self):
        self._con.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
        self._cursor.execute(
            sql.SQL(
                'CREATE TABLE {} ('
                '    id SERIAL PRIMARY KEY,'
                '    path text NOT NULL,'
                '    timestamp timestamp NOT NULL'
                ');'
            ).format(sql.Identifier(settings.TABLE_NAME))
        )
        self._con.set_isolation_level(ISOLATION_LEVEL_DEFAULT)

    def save_pic(self, path, timestamp):
        self._cursor.execute(
            sql.SQL(
                'INSERT INTO {} (path, timestamp) VALUES ({}, {});'
            ).format(
                sql.Identifier(settings.TABLE_NAME),
                sql.Literal(path),
                sql.Literal(timestamp),
            )
        )
        self._con.commit()

    def all(self):
        return self.filter().all()

    def first(self):
        return self.filter().first()

    def last(self):
        return self.filter().last()

    def filter(self, **kwargs):
        query = PycEry(self._user, self._password, self._host, self._dbname)
        for key in kwargs:
            query.set_query(key, kwargs[key])
        return query


class PycEry(PycDBBase):
    """
    Query object with method chaining
    """
    fields = {
        'id': [int],
        'path': [str],
        'timestamp': [datetime.date, datetime.datetime]
    }

    cmp_opts = {
        'lt': '<',
        'lte': '<=',
        'gt': '>',
        'gte': '>=',
    }

    date_opts = {
        'year': 'YEAR',
        'month': 'MONTH',
        'day': 'DAY',
        'hour': 'HOUR',
        'minute': 'MINUTE',
        'second': 'SECOND',
    }

    def __init__(self, user, password, host, dbname):
        super().__init__(user, password, host, dbname)
        self.connect_db()
        self._executed = False

        self._basequery = sql.SQL(
            'SELECT id, path, timestamp FROM {} '
        ).format(sql.Identifier(settings.TABLE_NAME))
        self._query = []

    def execute(self):
        if not self._executed:
            self._executed = True
            if self._query:
                query = sql.SQL('INTERSECT ').join(self._query)
            else:
                query = self._basequery
            self._cursor.execute(
                query + sql.SQL('ORDER BY timestamp')
            )

    def set_query(self, key, data):
        cond = key.split('__')
        conds = len(cond)
        date_conv = False

        if conds > 4:
            raise TypeError

        if conds == 1:  # e.g. field=data
            opt = '='
            if type(data) not in self.fields[cond[0]]:
                raise TypeError
            if type(data) is datetime.date:
                date_conv = True
            if cond[0] == 'timestamp':
                data = data.strftime(settings.TIMESTAMP_FORMAT)

        elif conds == 2:
            if cond[1] in self.cmp_opts:  # e.g. field__lte=data
                opt = self.cmp_opts[cond[1]]
                if type(data) not in self.fields[cond[0]]:
                    raise TypeError
                if type(data) is datetime.date:
                    date_conv = True
                if cond[0] == 'timestamp':
                    data = data.strftime(settings.TIMESTAMP_FORMAT)

            elif cond[1] in self.date_opts:  # e.g. timestamp__month=data
                cond[0] = 'EXTRACT ({} from timestamp)'.format(self.date_opts[cond[1]])
                opt = '='
                if type(data) is not int:
                    raise TypeError
            else:
                raise TypeError

        else:  # e.g. timestamp__month__lte=data
            if (cond[1] not in self.date_opts or cond[2] not in self.cmp_opts):
                raise TypeError

            cond[0] = 'EXTRACT ({} from timestamp)'.format(self.date_opts[cond[1]])
            opt = self.cmp_opts[cond[2]]
            if type(data) is not int:
                raise TypeError

        self._query.append(
            self._basequery + sql.SQL(
                'WHERE {field}{date_conv} {opt} {data}{date_conv} '
            ).format(
                field=sql.SQL(cond[0]),
                date_conv=sql.SQL('::DATE') if date_conv else sql.SQL(''),
                opt=sql.SQL(opt),
                data=sql.Literal(data),
            )
        )

    def all(self):
        self.execute()
        data = self._cursor.fetchall()
        self.close()
        return data

    def first(self):
        return self[0]

    def last(self):
        return self[-1]

    def filter(self, **kwargs):
        for key in kwargs:
            self.set_query(key, kwargs[key])
        return self

    def __getitem__(self, key):
        if type(key) is not int:
            raise KeyError

        self.execute()

        rowcount = self._cursor.rowcount
        if rowcount == 0 or rowcount <= key:
            return None
        if key < 0:
            key = key % rowcount

        rownumber = self._cursor.rownumber
        self._cursor.scroll(key, mode='absolute')
        data = self._cursor.fetchone()
        self._cursor.scroll(rownumber, mode='absolute')
        return data

    def __len__(self):
        self.execute()
        return self._cursor.rowcount

    def __bool__(self):
        return bool(len(self))

    def __iter__(self):
        self.execute()
        return self

    def __next__(self):
        data = self._cursor.fetchone()
        if data is None:
            raise StopIteration
        return data


if __name__ == '__main__':
    import datetime
    today = datetime.datetime(2018, 1, 1)
    db = PycDB(**settings.DB_OPTION).filter(timestamp__month=7)
    print(db.all())
