import multiprocessing

from loader import PycLoader
from manager import PycManager
from tk import PycTk


class PycCalendar(object):
    def __init__(self):
        self.manager = PycManager()
        self.cpu_queue = self.manager.PycQueue()
        self.io_queue = self.manager.PycQueue()
        self.tk = PycTk(self.cpu_queue, self.io_queue)

        cpu_count = multiprocessing.cpu_count()
        if cpu_count > 1:
            cpu_count - 1

        self.loaders = [
            PycLoader(self.cpu_queue, self.io_queue)
            for i in range(cpu_count)
        ]

    def run(self):
        for loader in self.loaders:
            loader.start()
        self.tk.run()
        for loader in self.loaders:
            loader.terminate()
