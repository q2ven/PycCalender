import queue
from multiprocessing.managers import BaseManager


class PycManager(BaseManager):
    def __init__(self):
        super().__init__()
        self.start()


class PycQueue(object):
    def __init__(self, *args, **kwargs):
        self.__list = list()
        self.__index = -1
        self.__insert_index = -1

    def get(self, block=True):
        if block:
            while not len(self.__list):
                pass

        try:
            data = self.__list.pop(self.__index)
            if self.__index == -1:
                self.__index = 0
            elif self.__index == 0:
                self.__index = len(self.__list) // 2
            else:
                self.__index = -1
            self.__insert_index = -1
            return data
        except IndexError:
            self.__index = -1
            raise queue.Empty

    def put(self, obj):
        self.__list.insert(self.__insert_index, obj)
        self.__insert_index -= 1

PycManager.register('PycQueue', PycQueue)
